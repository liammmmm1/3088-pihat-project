PiHat: LED light controller with sensor

Description:
	This Hat controlls a set of 3 LED lights. It has a Passive IR sensor to detect movement in the area and sends a signal to the Pi.
	The Hat recieves a 12-20V input range and will control 3 12V LED lamps, provide a logic input for the sensor and a trigger output to trigger simple machines.


Instructions:
	GPIO pins-
		1: Light 1 trigger
		2: Light 2 trigger
		3: Light 3 trigger
		4: Sensor digital input
		5: External switch trigger

	A logic high will trigger the light triggers.
	Sensor digital input will input a logic high when it detects movement.
	External switch trigger will enable on logic high.(Triiger can be used to switch low power devices or "on" buttons on systems)
